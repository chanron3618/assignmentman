# AssignmentMan

# Getting started

## Folder 

buat folder berikut : "D:/Chanron Sitanggang/Images/"

Pada disk D: tambahkan folder Chanron Sitanggang lalu dalam folder Chanron Sitanggang tambahkan folder Image

## Database

Buat Database dengan nama Man289

## Detail web developer

Ada 3 page yang dapat diakses pada project ini :
    1. Login page : http://localhost:8082
    2. User page : http://localhost:8082/users/index
    3. Transaksi page : http://localhost:8082/transaksi/index
Pada tampilan awal akan ditampilkan login page, anda dapat registrasi terlebih dahulu.
Ketika anda memasukan alamat email aktif, maka kode OTP akan dikirimkan ke email tersebut.

Setelah itu baru anda dapat login menggunakan username yang telah teregistrasi.
