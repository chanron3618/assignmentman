package minpro289.models;

import java.time.LocalDateTime;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="transaksi")
public class Transaksi {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	public M_user m_user;
	

	@Column(name="jenis_bank")
	private String jenis_bank;
	
	
	@Column(name="jumlah")
	private Long jumlah;
	
	@Column(name="name")
	private String name;
	

	@Column(name="norek")
	private Long norek;
	

	@Column(name="date")
	private LocalDateTime date;
	
	
	@Column(name="keterangan")
	private String keterangan;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getJenis_bank() {
		return jenis_bank;
	}


	public void setJenis_bank(String jenis_bank) {
		this.jenis_bank = jenis_bank;
	}


	public Long getJumlah() {
		return jumlah;
	}


	public void setJumlah(Long jumlah) {
		this.jumlah = jumlah;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Long getNorek() {
		return norek;
	}


	public void setNorek(Long norek) {
		this.norek = norek;
	}


	public LocalDateTime getDate() {
		return date;
	}


	public void setDate(LocalDateTime date) {
		this.date = date;
	}


	public String getKeterangan() {
		return keterangan;
	}


	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	
	
	
	
}
