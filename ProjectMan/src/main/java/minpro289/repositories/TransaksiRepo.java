package minpro289.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import minpro289.models.Transaksi;





@Repository
public interface TransaksiRepo extends JpaRepository<Transaksi, Long> {

}
