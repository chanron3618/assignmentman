package minpro289.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import minpro289.models.T_token;

@Repository
public interface TokenRepo extends JpaRepository<T_token, Long> {
	
	@Query(value="select * from t_token where t_token.token=?1", nativeQuery=true)
	List<T_token>getotepe(String otp);
	
	
	
	
}
