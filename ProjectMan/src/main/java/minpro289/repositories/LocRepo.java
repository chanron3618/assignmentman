package minpro289.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import minpro289.models.M_location;
@Repository
public interface LocRepo extends JpaRepository<M_location, Long> {

	@Query(value="select * from m_location where location_level_id =2 or location_level_id=3",
			nativeQuery=true)
	List<M_location> findkota();
}
