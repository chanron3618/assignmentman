package minpro289.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import minpro289.models.M_biodata_address;

@Repository
public interface AlamatRepo extends JpaRepository<M_biodata_address, Long> {

	@Query("FROM M_biodata_address Where Location_id=?1")
	List<M_biodata_address> findByLocation_id(Long Location_id);
	
	@Query(value="select * from m_biodata_address where is_delete=false", 
			nativeQuery=true)
	List<M_biodata_address> findisdelete();
	
	@Query(value="select * from m_location where location_level_id =2 or location_level_id=3",
			nativeQuery=true)
	List<M_biodata_address> findkota();
	
	@Query(value="select * from m_biodata_address where Label_biodata=?1", 
			nativeQuery=true)
	List<M_biodata_address>findlabel(String label);
	
	@Query(value="select distinct label_biodata from m_biodata_address", 
			nativeQuery=true)
	List<M_biodata_address> getlabel();
	
	@Query(value="select address=?1, recipient=?2  from m_biodata_address", nativeQuery=true)
	List<M_biodata_address> searchaddress(String address);
	
	@Query(value="select  recipient=?1  from m_biodata_address", nativeQuery=true)
	List<M_biodata_address> searchrecipient(String recipient);
}
