package minpro289.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import minpro289.models.M_user;

@Repository
public interface UserRepo extends JpaRepository<M_user, Long>{

	@Query(value="select * from m_user where m_user.email=?1 and m_user.password=?2",
			nativeQuery=true)
	List<M_user>getLogin(String Email, String Password);
	
	@Query(value="select * from m_user where m_user.email=?1", nativeQuery=true)
	List<M_user>getEmail(String Email);
	
	
	
	

	
	
}
