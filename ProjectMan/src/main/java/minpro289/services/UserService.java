package minpro289.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import minpro289.models.M_user;
import minpro289.repositories.UserRepo;

@Service
@Transactional
public class UserService {
	
	@Autowired UserRepo userepo;
	
	public List<M_user> listAll() {
		return this.userepo.findAll();
	}

}
