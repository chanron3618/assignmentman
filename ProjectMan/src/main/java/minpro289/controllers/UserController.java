package minpro289.controllers;


import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Stream;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import minpro289.models.M_biodata;
import minpro289.models.M_role;
import minpro289.models.M_user;
import minpro289.models.T_token;
import minpro289.repositories.BiodataRepo;
import minpro289.repositories.RoleRepo;
import minpro289.repositories.TokenRepo;
import minpro289.repositories.UserRepo;

@RestController
@CrossOrigin("*")
@Controller
@RequestMapping(value="/users/")
public class UserController {
	
	@Autowired UserRepo userepo;
	
	@Autowired TokenRepo tokenrepo;
	
	@GetMapping(value="index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/user/register");
		M_user user =new M_user();
		view.addObject("user", user);
		return view;
	}

	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/user/login");
		M_user user =new M_user();
		view.addObject("user", user);
		return view;
	}
	
	@GetMapping(value="lupapass")
	public ModelAndView lupapass() {
		ModelAndView view = new ModelAndView("/user/lupapass");
		M_user user = new M_user();
		view.addObject("user", user);
		return view;
	}
	
	@PostMapping(value="ceklogin")
	public ModelAndView ceklogin(
			@ModelAttribute M_user m_user,
			BindingResult result,
			HttpSession sees) {
		String redirect ="";
	
		String email =(String) result.getFieldValue("Email");
		String password=(String) result.getFieldValue("Password");
		
		List<M_user> getuser = this.userepo.getLogin(email, password);
		
		
		try {
			sees.setAttribute("id", getuser.get(0).getId());
			redirect = "redirect:/transaksi/index";
			
		}catch(Exception e) {
			
			redirect = "redirect:/users/form";
		}
		//tambahan coba coba
		ModelAndView view = new ModelAndView(redirect);
		M_user user =new M_user();
		view.addObject("user", user);
		//
		return view;//new ModelAndView(redirect);
	}
	
	@PostMapping(value="cekemail")
	public ModelAndView cekemail(
			@ModelAttribute M_user m_user,
			@ModelAttribute T_token t_token,
			BindingResult result,
			HttpSession sees) throws Exception {
		String redirect ="";
		
		if(!result.hasErrors()) {
		
		String email = (String) result.getFieldValue("Email");
		List<M_user> getuser = this.userepo.getEmail(email);
		
		try {	
			sees.setAttribute("uid", getuser.get(0).getId());
			redirect ="redirect:/"; //notif error email sudah terdaftar
		}catch(Exception e) {
			m_user.setCreated_by((long)1);
			LocalDateTime datetime = LocalDateTime.now();
			m_user.setCreated_on(datetime);
			this.userepo.save(m_user);
			
				System.out.println("Sending...");
				String otp = getOTP(6);
				sendmail(email,otp);
				System.out.println("Done!");
				
				t_token.setCreate_by((long)1);
				t_token.setCreate_on(datetime);
				t_token.setToken(otp);
				t_token.setIs_delete(false);
			
				this.tokenrepo.save(t_token);
				
			redirect="redirect:/otp/index";
		}
	}
		return new ModelAndView(redirect);
		
	}
	
	@GetMapping(value="password")
	public ModelAndView pass() {
		ModelAndView view = new ModelAndView("/user/password");
		M_user user =new M_user();
		view.addObject("user", user);
		return view;
	}
	
	@PostMapping(value="setpassword")
	public ModelAndView setpass(@ModelAttribute M_user m_user,
			BindingResult result) {
		if(!result.hasErrors()) {
			String password = result.getFieldValue("Password").toString();
			m_user.setCreated_by((long)1);
			LocalDateTime datetime = LocalDateTime.now();
			m_user.setCreated_on(datetime);
			m_user.setPassword(password);
			this.userepo.save(m_user);
		}
		return new ModelAndView("redirect:/users/signup");
	}

	@Autowired BiodataRepo biorepo;
	@Autowired RoleRepo rolerepo;
	
	@GetMapping(value="signup")
	public ModelAndView signup() {
		ModelAndView view = new ModelAndView("/user/signup");
		M_biodata biodata = new M_biodata();
		view.addObject("biodata", biodata);
		M_user user = new M_user();
		view.addObject("user", user);
		List<M_role> role = this.rolerepo.findAll();
		view.addObject("role", role);
		return view;
	}
	
	@PostMapping(value="setsignup")
	public ModelAndView setsignup(
			@ModelAttribute M_user m_user,
			@ModelAttribute M_biodata m_biodata,
			BindingResult result) {
		if(!result.hasErrors()) {
			String fullname =result.getFieldValue("Fullname").toString();
			String nohp = result.getFieldValue("Mobile_phone").toString();
			
				m_biodata.setCreated_by((long)1);
				LocalDateTime datetime = LocalDateTime.now();
				m_biodata.setCreated_on(datetime);
				m_biodata.setFullname(fullname);
				m_biodata.setMobile_phone(nohp);
				this.biorepo.save(m_biodata);
				
				String role = result.getFieldValue("Role_id").toString(); //yang bikin error dude
				m_user.setCreated_by((long)1);
				m_user.setCreated_on(datetime);
				m_user.setRole_id(Long.parseLong(role));
				this.userepo.save(m_user);
			
			
		}
		return new ModelAndView("redirect:/transaksi/index");
	}
	
	@PostMapping(value="emailpass")
	public ModelAndView lupapass(
			@ModelAttribute M_user m_user,
			@ModelAttribute T_token t_token,
			BindingResult result,
			HttpSession sees) {
		String redirect ="";
		if(!result.hasErrors()) {
			String email =(String) result.getFieldValue("Email");
			
			List<M_user> getuser = this.userepo.getEmail(email);
			
			try {
				sees.setAttribute("id", getuser.get(0).getId());
				m_user.setCreated_by((long)1);
				LocalDateTime datetime = LocalDateTime.now();
				m_user.setCreated_on(datetime);
				this.userepo.save(m_user);
				
					System.out.println("Sending...");
					String otp = getOTP(6);
					sendmail(email,otp);
					System.out.println("Done!");
					
					t_token.setCreate_by((long)1);
					t_token.setCreate_on(datetime);
					t_token.setToken(otp);
					t_token.setIs_delete(false);
				
					this.tokenrepo.save(t_token);
					redirect ="redirect:/otp/index";
			}catch(Exception e) {
				redirect = "redirect:/users/index";
			}
			
		}
		return new ModelAndView(redirect);
	}
	
	
	
	@Autowired private JavaMailSender javamailsender;
	public void sendmail(String emailto,String otp) throws Exception{
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setFrom("chanron3618@gmail.com");
		msg.setTo(emailto);
		msg.setSubject("Hallo "+ emailto + " Kamu telah berhasil mendaftar");
		msg.setText("OTPnya ini = "+ otp);
		javamailsender.send(msg);
	}
	
	static String getOTP(int n) {
		String getOTP ="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                		+ "0123456789"
                		+ "abcdefghijklmnopqrstuvxyz";

		StringBuilder sb = new StringBuilder(n);
		
		for(int i =0; i<n; i++) {
			int index = (int)(getOTP.length()*Math.random());
			
			sb.append(getOTP.charAt(index));
		}
		return sb.toString();
	}
	
	
	
	
	
}
	


