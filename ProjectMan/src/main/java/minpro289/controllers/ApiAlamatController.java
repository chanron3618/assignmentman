package minpro289.controllers;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import minpro289.models.M_biodata_address;
import minpro289.repositories.AlamatRepo;

@RestController
@RequestMapping(value="/api/")
public class ApiAlamatController {
	
	@Autowired AlamatRepo alamatrepo;
	
	@GetMapping("alamat/{label}")
	public ResponseEntity<Stream<M_biodata_address>>getLabel(@PathVariable("label") String label){
		try {
			List<M_biodata_address>m_biodata_user=this.alamatrepo.findlabel(label);
			Stream<M_biodata_address>user =m_biodata_user.stream();
			return new ResponseEntity<Stream<M_biodata_address>>(user,HttpStatus.OK);			
		}catch(Exception e) {
			return new ResponseEntity<Stream<M_biodata_address>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("bioalamat/{id}")
	public ResponseEntity<M_biodata_address>getId(@PathVariable("id") Long id){
		try {
			M_biodata_address m_biodata_address = this.alamatrepo.findById(id).orElse(null);
			return new ResponseEntity<M_biodata_address>(m_biodata_address,HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<M_biodata_address>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("search/{alama}")
	public ResponseEntity<Stream<M_biodata_address>>Search(@PathVariable("alama") String alama){
		try {
			List<M_biodata_address>searchaddress=this.alamatrepo.searchaddress(alama);
			Stream<M_biodata_address>search = searchaddress.stream();
			return new ResponseEntity<Stream<M_biodata_address>>(search,HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<Stream<M_biodata_address>>(HttpStatus.NO_CONTENT);
		}
	}
	@GetMapping("search/{name}")
	public ResponseEntity<Stream<M_biodata_address>>Searchname(@PathVariable("name")String name){
		try {
			List<M_biodata_address>search=this.alamatrepo.searchrecipient(name);
			Stream<M_biodata_address>searchname = search.stream();
			return new ResponseEntity<Stream<M_biodata_address>>(searchname, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<Stream<M_biodata_address>>(HttpStatus.NO_CONTENT);
		}
	}
	

}
