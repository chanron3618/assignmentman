package minpro289.controllers;

import java.time.LocalDateTime;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.servlet.ModelAndView;


import minpro289.models.M_biodata_address;
import minpro289.models.M_location;
import minpro289.repositories.AlamatRepo;

import minpro289.repositories.LocRepo;


@Controller
@RequestMapping(value="/alamat/")
public class AlamatController {

	@Autowired
	private AlamatRepo alamatrepo;
	
	@Autowired
	private LocRepo locrepo;
	
	
	
	@GetMapping(value="index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/alamat/index");
		M_biodata_address m_biodata_address = new M_biodata_address();
		view.addObject("m_biodata_address", m_biodata_address);

		List<M_biodata_address> listalamat = this.alamatrepo.findisdelete();
		view.addObject("listalamat", listalamat);
		
		//List<M_biodata_address> listlabel = this.alamatrepo.getlabel();
		//view.addObject("listlabel", listlabel);
		
		return view;
	}
	
	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/alamat/form");
		M_biodata_address m_biodata_address = new M_biodata_address();
		view.addObject("m_biodata_address", m_biodata_address);
		List<M_location> listlocation = this.locrepo.findkota();
		view.addObject("listlocation", listlocation);
		return view;
	}
	
	@PostMapping(value="save")
	public ModelAndView save(@ModelAttribute M_biodata_address m_biodata_address, BindingResult result) {
		if(!result.hasErrors()) {
			m_biodata_address.setCreated_by((long)1);
			LocalDateTime datetime = LocalDateTime.now();
			m_biodata_address.setCreated_on(datetime);
			this.alamatrepo.save(m_biodata_address);
		}
		return new ModelAndView("redirect:/alamat/index");
		}

	@GetMapping(value="/delete/{id}")
	public ModelAndView deleteform(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/alamat/deleteform");
		M_biodata_address m_biodata_address = this.alamatrepo.findById(id).orElse(null);
		view.addObject("m_biodata_address", m_biodata_address);
		return view;
	}
	
	@GetMapping(value="/del/{id}")
	public ModelAndView del(
			@PathVariable("id") Long id) {
		if(id != null) {
			ModelAndView view = new ModelAndView("/delete/{id}");
			M_biodata_address address = this.alamatrepo.findById(id).orElse(null);
			view.addObject("address", address);
			address.setIs_delete(true);
			this.alamatrepo.save(address);
		}
		
		return new ModelAndView ("redirect:/alamat/index");
	}
	
	@GetMapping(value="/editform/{id}")
	public ModelAndView editform(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/alamat/form");
		M_biodata_address m_biodata_address = this.alamatrepo.findById(id).orElse(null);
		view.addObject("m_biodata_address", m_biodata_address);
		List<M_location> listlocation = this.locrepo.findkota();
		view.addObject("listlocation", listlocation);
		return view;
	}
}
