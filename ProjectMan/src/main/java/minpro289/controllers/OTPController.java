package minpro289.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import minpro289.models.T_token;
import minpro289.repositories.TokenRepo;

@RestController
@CrossOrigin("*")
@Controller
@RequestMapping(value="/otp/")
public class OTPController {
	
	@Autowired TokenRepo tokenrepo;
	
	@GetMapping(value="index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/user/otp");
		T_token token = new T_token();
		view.addObject("token", token);
		return view;
	}
	
	@PostMapping(value="cekotp")
	public ModelAndView cekotp(
			@ModelAttribute T_token t_token,
			BindingResult result,
			HttpSession sees) {
		String redirect="";
		String otp = (String) result.getFieldValue("Token");
		List<T_token> getotp = this.tokenrepo.getotepe(otp);
		try {
			sees.setAttribute("uid", getotp.get(0).getId());
			redirect = "redirect:/users/password";
		}catch(Exception e) {
			redirect = "redirect:/otp/index";
		}
		return new ModelAndView(redirect);
	}
		
	
}
