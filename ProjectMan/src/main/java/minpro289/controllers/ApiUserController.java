package minpro289.controllers;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import minpro289.models.M_user;
import minpro289.repositories.UserRepo;

@RestController
@RequestMapping(value="/api/")
public class ApiUserController {

	@Autowired UserRepo userepo;
	
	@GetMapping("user")
	public ResponseEntity<Stream<M_user>>getAllUser(){
		try {
			List<M_user>userlist=this.userepo.findAll();
			Stream<M_user>user = userlist.stream();
			return new ResponseEntity<Stream<M_user>>(user, HttpStatus.OK);			
		}catch(Exception e) {
			return new ResponseEntity<Stream<M_user>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("(user/{emailsearch}")
	public ResponseEntity<Stream<M_user>>findEmail(
			@PathVariable("emailsearch")String emailsearch){
		try {
			List<M_user> user = this.userepo.getEmail(emailsearch);
			Stream<M_user> m_user = user.stream(); 
			return new ResponseEntity<Stream<M_user>>(m_user,HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<Stream<M_user>>(HttpStatus.NO_CONTENT);
		}
	}
}
