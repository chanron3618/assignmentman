package man9.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import man9.models.Transaksi;
import man9.repositories.TransaksiRepo;



@Controller
@RequestMapping(value="/transaksi/")
public class TransaksiController {

	@Autowired
	private TransaksiRepo transrepo;
	
	@GetMapping(value="index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/transaksi/index");
		List<Transaksi> listtrans = this.transrepo.findAll();
		view.addObject("listtrans", listtrans);
		return view;
	}
	
	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/transaksi/form");
		Transaksi transaksi = new Transaksi();
		view.addObject("transaksi", transaksi);
		return view;
	}
	
	@PostMapping(value="save")
	public ModelAndView save(@ModelAttribute Transaksi transaksi, BindingResult result) {
		LocalDateTime date = LocalDateTime.now();
		if(!result.hasErrors()) {
			transaksi.setDate(date);
			String hasil = result.getFieldValue("jumlah").toString();
			transaksi.setJumlah(Long.parseLong(hasil)+ 1000);
			this.transrepo.save(transaksi);
		
		}
		return new ModelAndView("redirect:/transaksi/index");
	}

}
