package man9.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import man9.models.Transaksi;



@Repository
public interface TransaksiRepo extends JpaRepository<Transaksi, Long> {

}
