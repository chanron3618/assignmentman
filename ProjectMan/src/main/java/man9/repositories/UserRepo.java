package man9.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import man9.models.Users;



@Repository
public interface UserRepo extends JpaRepository<Users, Long> {

	@Query(value="select * from users where users.username= ?1 and users.password=?2",
			nativeQuery=true)
	List<Users>getLogin(String UserName, String Password);
}
