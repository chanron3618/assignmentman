package man9.models;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="transaksi")
public class Transaksi {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	public Users User_id;
	

	@Column(name="jenis_bank")
	private String jenis_bank;
	
	
	@Column(name="jumlah")
	private Long jumlah;
	
	public Long getJumlah() {
		return jumlah;
	}

	public void setJumlah(Long jumlah) {
		this.jumlah = jumlah;
	}

	
	@Column(name="name")
	private String name;
	

	@Column(name="norek")
	private Long norek;
	

	@Column(name="date")
	private LocalDateTime date;
	
	
	@Column(name="keterangan")
	private String keterangan;
	
	private static final long OTP_VALID_DURATION = 5 * 60 * 1000;
	
	@Column(name="one_time_password")
	private String oneTimePassword;
	
	@Column(name="otp_requested_time")
	private Date otpRequestedTime;
	
	public boolean isOTPRequired() {
		if(this.getOneTimePassword()==null) {
			return false;
		}
		long currentTimeInMillis = System.currentTimeMillis();
		long otpRequestedTimeInMillis = this.otpRequestedTime.getTime();
		
		if(otpRequestedTimeInMillis + OTP_VALID_DURATION < currentTimeInMillis) {
			return false;
		}
		return true;
	}
	
	public String getOneTimePassword() {
		return oneTimePassword;
	}

	public void setOneTimePassword(String oneTimePassword) {
		this.oneTimePassword = oneTimePassword;
	}

	public Date getOtpRequestedTime() {
		return otpRequestedTime;
	}

	public void setOtpRequestedTime(Date otpRequestedTime) {
		this.otpRequestedTime = otpRequestedTime;
	}

	public static long getOtpValidDuration() {
		return OTP_VALID_DURATION;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getJenis_bank() {
		return jenis_bank;
	}

	public void setJenis_bank(String jenis_bank) {
		this.jenis_bank = jenis_bank;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getNorek() {
		return norek;
	}

	public void setNorek(Long norek) {
		this.norek = norek;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	
	
}
