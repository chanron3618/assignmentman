package man9.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

@Entity
@Table(name="users")
public class Users {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long Id;
	
	@Column(name="email", length=200)
	private String Email;
	
	@NotNull
	@Column(name="username", length=200)
	private String UserName;
	
	@NotNull
	@Column(name="password", length=200)
	private String Password;
	
	@NotNull
	@Column(name="fullname",length = 200)
	private String FullName;
	
	@NotNull
	@Column(name="photo")
	private String Photo;
	
	@Nullable
	@Column(name="otp")
	private String OTP;
	
	@Nullable
	@Column(name="otp_time")
	private Date Otptime;
	
	private static final long OTP_VALID_DURATION = 1 * 60 * 1000;
	
	public boolean isOTPRequired() {
		if(this.getOTP()==null) {
			return false;
		}
		long currentTimeinMillis = System.currentTimeMillis();
		long otpReqTimeinMillis = this.Otptime.getTime();
		
		if(otpReqTimeinMillis + OTP_VALID_DURATION < currentTimeinMillis) {
		return false;	
		}
		return true;
			
	}
	
	

	public Date getOtptime() {
		return Otptime;
	}

	public void setOtptime(Date otptime) {
		Otptime = otptime;
	}

	public String getOTP() {
		return OTP;
	}

	public void setOTP(String oTP) {
		OTP = oTP;
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getFullName() {
		return FullName;
	}

	public void setFullName(String fullName) {
		FullName = fullName;
	}

	public String getPhoto() {
		return Photo;
	}

	public void setPhoto(String photo) {
		Photo = photo;
	}


	
}
