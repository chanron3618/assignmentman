package man9;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectManApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectManApplication.class, args);
	}

}
